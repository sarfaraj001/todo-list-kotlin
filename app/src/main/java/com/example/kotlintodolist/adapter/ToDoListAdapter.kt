package com.example.kotlintodolist.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.kotlintodolist.R
import com.example.kotlintodolist.fragment.HomeFragment
import com.example.kotlintodolist.model.DetailObject
import io.realm.RealmResults
import kotlinx.android.synthetic.main.list_item.view.*

class ToDoListAdapter(var objectsDetail: RealmResults<DetailObject>?, val context: HomeFragment) :
    RecyclerView.Adapter<ToDoListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.list_item,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return objectsDetail!!.size
    }

    fun refresh(titleListArray: RealmResults<DetailObject>) {
        objectsDetail = titleListArray
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder?.etItem?.setText(objectsDetail?.get(position)?.titleData.toString())
        holder?.etItem?.setOnClickListener {
            objectsDetail?.get(position)?.id?.let { it1 -> context.openClickedItem(it1) }
        }
        holder.ivDelete.setOnClickListener {
            objectsDetail?.get(position)?.id?.let { it1 -> context.deleteOnClick(it1) }
        }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var etItem = view.tv_list_item
        var ivDelete = view.delete_item
    }

}