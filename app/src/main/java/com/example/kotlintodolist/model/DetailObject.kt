package com.example.kotlintodolist.model

import io.realm.RealmObject

open class DetailObject : RealmObject() {
    var id: Int = 0
    var titleData: String = ""
    var itemDetail: String = ""
}