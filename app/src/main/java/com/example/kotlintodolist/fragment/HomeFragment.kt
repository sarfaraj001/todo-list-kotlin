package com.example.kotlintodolist.fragment


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.NavHostFragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.kotlintodolist.R
import com.example.kotlintodolist.adapter.ToDoListAdapter
import com.example.kotlintodolist.model.DetailObject
import com.google.android.material.floatingactionbutton.FloatingActionButton
import io.realm.Realm
import io.realm.RealmResults

/**
 * A simple [Fragment] subclass.
 */
class HomeFragment : Fragment() {
    var customAdapter: ToDoListAdapter? = null
    internal lateinit var views: View
    var realm: Realm = Realm.getDefaultInstance();
    internal var updateId: Int = 0

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment`
        views = inflater.inflate(R.layout.fragment_home, container, false)
        val addItem = views.findViewById<FloatingActionButton>(R.id.fb_add_item)
        addItem.setOnClickListener {
            NavHostFragment.findNavController(this).navigate(R.id.action_nav_home_to_nav_todo)
        }
        fetchData()
        return views
    }

    private fun fetchData() {
        if (!realm.isClosed) {
            val realmresult = realm.where(DetailObject::class.java).findAll()

            initToDoRecycler(realmresult)
        } else {
            Realm.getDefaultInstance()
        }
    }

    private fun initToDoRecycler(realmresult: RealmResults<DetailObject>?) {
        if (realmresult != null && realmresult.size >= 1) {
            val linearLayoutManager = LinearLayoutManager(context)
            val recyclerview = views.findViewById<RecyclerView>(R.id.recyclerview)
            recyclerview?.setLayoutManager(linearLayoutManager)
            customAdapter = ToDoListAdapter(realmresult, this)
            recyclerview?.setAdapter(customAdapter)
        }
    }

    fun deleteOnClick(id: Int) {
        updateId = id
        if (updateId == 0) {
            return
        }
        try {
            if (realm.isClosed) {
                realm = Realm.getDefaultInstance()
            }
            realm.executeTransactionAsync(Realm.Transaction { realm ->
                if (updateId != 0) {
                    val detailObject =
                        realm.where(DetailObject::class.java).equalTo("id", updateId).findFirst()
                    detailObject!!.deleteFromRealm()
                }
            }, Realm.Transaction.OnSuccess {
                if (realm.isClosed) {
                    realm = Realm.getDefaultInstance()
                }
                val results = realm.where(DetailObject::class.java).findAll()
                customAdapter?.refresh(results)
            })

        } catch (e: Exception) {
            Toast.makeText(
                context,
                "connection error delete : " + e.localizedMessage,
                Toast.LENGTH_SHORT
            ).show()
        } finally {

            Toast.makeText(context, "Deleted", Toast.LENGTH_SHORT).show()
            if (!realm.isClosed) {
                realm.close()
            }
        }
    }

    fun openClickedItem(getId: Int) {
        val bundle = Bundle()
        bundle.putInt("ID", getId)
        NavHostFragment.findNavController(this)
            .navigate(R.id.action_nav_home_to_nav_todo, bundle)
    }

    override fun onDestroy() {
        super.onDestroy()
        if (!realm.isClosed) {
            realm.close()
        }
    }

    override fun onResume() {
        super.onResume()
        if (!realm.isClosed) {
            realm = Realm.getDefaultInstance()
        }
        val realmresult = realm.where(DetailObject::class.java).findAll()
        customAdapter?.refresh(realmresult)

    }
}
