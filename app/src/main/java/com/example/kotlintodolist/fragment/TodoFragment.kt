package com.example.kotlintodolist.fragment

import android.content.ContentValues.TAG
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.NavHostFragment
import com.example.kotlintodolist.R
import com.example.kotlintodolist.activity.MainActivity
import com.example.kotlintodolist.model.DetailObject
import io.realm.Realm
import kotlinx.android.synthetic.main.fragment_todo.*


/**
 * A simple [Fragment] subclass.
 */
class TodoFragment : Fragment() {
    var idNumber: Int = 0
    internal lateinit var views: View
    var realm: Realm = Realm.getDefaultInstance()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        views = inflater.inflate(R.layout.fragment_todo, container, false)
        view
        initView(views)
        return views
    }

    private fun initView(view: View) {
        val save = view.findViewById(R.id.save) as Button
        save.setOnClickListener {
            if(tvTitle.text.isEmpty())
            {
                Toast.makeText(context,"please enter Title",Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            insertToDB()
        }

        if (arguments != null) {
            idNumber = arguments!!.getInt("ID", 0)
            displayTodoList(idNumber)
        }
    }

    private fun insertToDB() {
        Log.d(TAG, "inside insert database")
        try {
            if (realm.isClosed) {
                realm = Realm.getDefaultInstance()
            }
            realm.executeTransaction { realm ->
                var detailObject: DetailObject?
                if (idNumber != 0) {
                    detailObject =
                        realm.where(DetailObject::class.java).equalTo("id", idNumber).findFirst()
                } else {
                    detailObject = DetailObject()
                    val number = realm.where(DetailObject::class.java).max("id")
                    if (number == null) {
                        detailObject.id = 1
                    } else {
                        detailObject.id = number.toInt() + 1
                    }
                }

                if (detailObject != null) {
                    detailObject.titleData = tvTitle.text.toString()
                    detailObject.itemDetail = detail.text.toString()
                    realm.insertOrUpdate(detailObject)
                }
            }
        } catch (e: Exception) {
            Toast.makeText(
                context, "connection error 2 : " + e.localizedMessage, Toast.LENGTH_SHORT
            ).show()
        } finally {
            (activity as MainActivity).hideKeyboard()
            Toast.makeText(context, "updated", Toast.LENGTH_SHORT).show()
            if (!realm.isClosed) {
                realm.close()
                Log.d(TAG, "insertToDB: completed")
            }
            clearField()
            NavHostFragment.findNavController(this).popBackStack()
        }
    }

    private fun clearField() {
        Log.d(TAG, "clearField: starting")
        detail.setText("")
        tvTitle.setText("")
    }

    private fun displayTodoList(updateId: Int) {
        Log.d("check", "exicuted ")
        if (updateId == 0) {
            return
        }
        try {
            if (realm.isClosed() || realm == null) {
                realm = Realm.getDefaultInstance()
            }
            realm.executeTransaction(Realm.Transaction { realm ->
                var fatchTitle = ""
                var fatchDetail = ""
                val person = realm.where(DetailObject::class.java!!).equalTo("id", updateId).findFirst()

                fatchDetail = person!!.itemDetail
                fatchTitle = person!!.titleData

            val     tvTitle = views.findViewById(R.id.tvTitle) as EditText
            val    detail = views.findViewById(R.id.detail) as EditText
                tvTitle.setText(fatchTitle)
                detail.setText(fatchDetail)
            })

        } catch (e: Exception) {
            Toast.makeText(context, "Date is not fatching : " + e.localizedMessage, Toast.LENGTH_SHORT).show()

        } finally {
            if (realm != null && !realm.isClosed()) {
                realm.close()
                Log.d(TAG, "Showed data: completed")
            }
        }
    }

}

